/**
 * Created by abhishekrathore on 1/30/17.
 */
/**
 * Module dependencies.
 */

var express = require('express');
var _ = require('underscore');
var wordApi = require('./word.js');



var app = module.exports = express();

// Configuration

app.configure(function(){
    app.set('views','cloud/views');
    app.set('view engine', 'ejs');
    app.set('view options', {
        layout: false
    });

    app.use(function (req, res, next) {

        res.set('Expires','Thu, 15 Apr 2014 20:00:00 GMT' );
        res.set('Cache-Control','public,max-age=1209600');
        console.log("middlewares:"+req.url);

        next();

    });

    app.use(express.bodyParser());
    app.use(express.methodOverride());

    app.use(app.router);

});


app.configure('development', function(){
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});


// Clicking submit on the login form triggers this.





//app.configure('production', function(){
//    app.use(express.errorHandler());
//});

// Routes

//app.get("/",routes.index);
//app.get('*',routes.maintenance);
//app.get('*/*',routes.maintenance);



app.post('/api/wordUpdate',wordApi.wordUpdate);
app.post('/api/deleteWord',wordApi.deleteWord);



// Required for initializing Express app in Cloud Code.
app.listen();

