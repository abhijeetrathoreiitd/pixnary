/**
 * Created by abhishekrathore on 1/30/17.
 */

Parse.Cloud.job("wordListJob", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();
    var counter = 0;
    // Query for all users
    var Word =  Parse.Object.extend("Word");
    var wordQuery = new Parse.Query(Word);
    wordQuery.equalTo("gre");
    wordQuery.descending('createdAt');
    wordQuery.limit(437);

    wordQuery.find().then(function(results) {
        // Update to plan value passed in
        var word,i,promises = [];
        for(i=0;i<results.length;i++){
            word = results[i];
            word.set("wordlist", 'gre');
            word.set("sublist", '800');
            promises.push(word.save());
        }
        return Parse.Promise.when(promises);
    }).then(function() {
        // Set the job's success status
        status.success("Migration completed successfully.");
    }, function(error) {
        // Set the job's error status
        console.log(error);
        status.error("Uh oh, something went wrong.");
    });
});