/**
 * Created by abhishekrathore on 1/30/17.
 */
exports.wordUpdate = function (req, res) {
    console.log(req.body.word);

    var word = req.body.word;
    var Word = Parse.Object.extend("Word");
    var WordHasSentence = Parse.Object.extend("WordHasSentence");
    var Sentence = Parse.Object.extend("Sentence");
    var Image = Parse.Object.extend("Image");
    var WordHasImage = Parse.Object.extend("WordHasImage");

    var wordQuery = new Parse.Query(Word);
    var sentenceQuery = new Parse.Query(Sentence);

    wordQuery.equalTo("objectId",word.wordId);

    var promise1 =wordQuery.first().then(function(object){
        object.set("phrase", word.phrase);
        object.set("primaryMeaning", word.pmeaning);
        object.set("wordlist", word.wordlist);
        object.set("sublist", word.sublist);
        if (word.smeaning) {
            object.set("secondaryMeaning", word.smeaning);
        }
        return object.save();
    });

    sentenceQuery.equalTo("objectId",word.sentenceId);

    var promise2 =sentenceQuery.first().then(function(object){
        var content = {};
        content.text = word.sentence.text;
        object.set("content", content);
        return object.save();
    });

    Parse.Promise.when([promise1,promise2]).then(function(){

        res.json({"result":"success"})

    });


};

exports.deleteWord = function (req, res) {



    var word = req.body.word;
    var Word = Parse.Object.extend("Word");
    var WordHasSentence = Parse.Object.extend("WordHasSentence");
    var Sentence = Parse.Object.extend("Sentence");
    var Image = Parse.Object.extend("Image");
    var WordHasImage = Parse.Object.extend("WordHasImage");

    var wordQuery = new Parse.Query(Word);
    var sentenceQuery = new Parse.Query(Sentence);
    var whsQuery = new Parse.Query(WordHasSentence);
    var whiQuery = new Parse.Query(WordHasImage);
    var imageQuery = new Parse.Query(Image);

    wordQuery.equalTo("objectId",word.wordId);

    var promise1 =wordQuery.first().then(function(object){

        return object.destroy();
    });



    sentenceQuery.equalTo("objectId",word.sentenceId);

    var promise2 =sentenceQuery.first().then(function(object){

        return object.destroy();
    });

    whsQuery.equalTo("wordId",word.wordId);
    whiQuery.equalTo("wordId",word.wordId);
    imageQuery.matchesKeyInQuery("objectId","imageId",whiQuery);

    var promise3 =whsQuery.first().then(function(object){

        return object.destroy();
    });


    var promise5 =imageQuery.first().then(function(object){

        return object.destroy();
    });


    Parse.Promise.when([promise1,promise2,promise3,promise5]).then(function(){

        return whiQuery.first().then(function(object){

            return object.destroy();
        });

    }).then(function(){
        res.json({"result":"success"})

    });

};



