
var _ = require('underscore');
// require('./app.js');
// require('./jobs.js');

Parse.Cloud.define("getData",function(req,res){

  var number = req.params.number;
  var lastFetch = req.params.lastFetch;
  var wordData;
  var wordHasImageData;
  var imageData;
  var wordHasSentenceData;
  var sentenceData;
  var promise1,promise2,promise3,promise4,promise5;

  var Word =  Parse.Object.extend("Word");
  var WordHasSentence = Parse.Object.extend("WordHasSentence");
  var WordHasImage = Parse.Object.extend("WordHasImage");
  var Sentence = Parse.Object.extend("Sentence");
  var Image = Parse.Object.extend("Image");

  var wordQuery = new Parse.Query(Word);
  var wordHasImageQuery = new Parse.Query(WordHasImage);
  var wordHasSentenceQuery = new Parse.Query(WordHasSentence);
  var imageQuery = new Parse.Query(Image);
  var sentenceQuery = new Parse.Query(Sentence);
  wordQuery.equalTo("wordlist","gre");
  wordQuery.ascending('createdAt');
  wordQuery.skip(number);
  var limit = 9999;

  console.log("DATA0");

  wordQuery.limit(limit);
  promise1 = wordQuery.find({ useMasterKey: true }).then(function(results){
    wordData = results;
  });
  console.log("DATA1");

  wordHasSentenceQuery.matchesKeyInQuery("wordId","objectId",wordQuery);
  wordHasSentenceQuery.limit(limit);

  promise2 = wordHasSentenceQuery.find({ useMasterKey: true }).then(function(results){
    wordHasSentenceData = results;

  });

  sentenceQuery.matchesKeyInQuery("objectId","sentenceId",wordHasSentenceQuery);
  sentenceQuery.limit(limit);
  promise3 = sentenceQuery.find({ useMasterKey: true }).then(function(results){
    sentenceData = results;

  });


  wordHasImageQuery.matchesKeyInQuery("wordId","objectId",wordQuery);
  wordHasImageQuery.limit(limit);
  promise4 = wordHasImageQuery.find({ useMasterKey: true }).then(function(results){
    wordHasImageData = results;

  });

  imageQuery.matchesKeyInQuery("objectId","imageId",wordHasImageQuery);
  imageQuery.limit(limit);
  promise5 = imageQuery.find({ useMasterKey: true }).then(function(results){
    imageData = results;

  });

  Parse.Promise.when([promise1,promise2,promise3,promise4,promise5]).then(function(){

    var temp1 = _.map(wordData,function(word){
      if(typeof _.find(wordHasImageData,function(whi){ return whi.get("wordId")==word.id;}) == "undefined"){
        console.log(word.get("word"));
      }


      return {
        "wordId":word.id,
        "word":word.get("word"),
        "pmeaning":word.get("primaryMeaning"),
        "smeaning":word.get("secondaryMeaning"),
        "phrase":word.get("phrase"),
        "sublist":word.get("sublist"),
        "createdAt":word.createdAt,
        "updatedAt":word.updatedAt,
        "sentenceId": _.find(wordHasSentenceData,function(whs){ return whs.get("wordId")==word.id;}).get("sentenceId"),
        "imageId": _.find(wordHasImageData,function(whi){ return whi.get("wordId")==word.id;}).get("imageId")

      };
    });



    var temp2 = _.map(temp1,function(word){
      return {
        "wordId":word.wordId,
        "word":word.word,
        "pmeaning":word.pmeaning,
        "smeaning":word.smeaning,
        "sublist":word.sublist,
        "phrase":word.phrase,
        "sentenceId":word.sentenceId,
        "createdAt":word.createdAt,
        "updatedAt":word.updatedAt,
        "sentence": _.find(sentenceData,function(sentence){
          return sentence.id == word.sentenceId;
        }).get("content").text,
        "imageUrl":"https://secure-gorge-72882.herokuapp.com/public/images/"+word.word+".jpg"

      }
    });


    res.success(JSON.stringify(temp2));

  },function(err){
    res.error(err);
    console.log(err);
  });


});




Parse.Cloud.define("getDataJSON",function(req,res){

  var wordData;
  var number = req.params.number;
  var list = req.params.list;
  if(typeof list == 'undefined'){
    list=3000;
  }
  var wordHasImageData;
  var imageData;
  var wordHasSentenceData;
  var sentenceData;
  var promise1,promise2,promise3,promise4,promise5;

  var Word =  Parse.Object.extend("Word");
  var WordHasSentence = Parse.Object.extend("WordHasSentence");
  var WordHasImage = Parse.Object.extend("WordHasImage");
  var Sentence = Parse.Object.extend("Sentence");
  var Image = Parse.Object.extend("Image");

  var wordQuery = new Parse.Query(Word);
  var wordHasImageQuery = new Parse.Query(WordHasImage);
  var wordHasSentenceQuery = new Parse.Query(WordHasSentence);
  var imageQuery = new Parse.Query(Image);
  var sentenceQuery = new Parse.Query(Sentence);
  wordQuery.equalTo("sublist",list);
  wordQuery.ascending('createdAt');
  wordQuery.skip((number-1)*100);
  wordQuery.limit(100);




  promise1 = wordQuery.find({ useMasterKey: true }).then(function(results){
    wordData = results;
  });

  wordHasSentenceQuery.matchesKeyInQuery("wordId","objectId",wordQuery);
  wordHasSentenceQuery.limit(999);

  promise2 = wordHasSentenceQuery.find({ useMasterKey: true }).then(function(results){
    wordHasSentenceData = results;
  });

  sentenceQuery.matchesKeyInQuery("objectId","sentenceId",wordHasSentenceQuery);
  sentenceQuery.limit(999);

  promise3 = sentenceQuery.find({ useMasterKey: true }).then(function(results){
    sentenceData = results;
  });


  wordHasImageQuery.matchesKeyInQuery("wordId","objectId",wordQuery);
  wordHasImageQuery.limit(999);

  promise4 = wordHasImageQuery.find({ useMasterKey: true }).then(function(results){
    wordHasImageData = results;
  });

  imageQuery.matchesKeyInQuery("objectId","imageId",wordHasImageQuery);
  imageQuery.limit(999);

  promise5 = imageQuery.find({ useMasterKey: true }).then(function(results){
    imageData = results;
  });

  Parse.Promise.when([promise1,promise2,promise3,promise4,promise5]).then(function(){

    var temp1 = _.map(wordData,function(word){
      return {
        "wordId":word.id,
        "word":word.get("word"),
        "pmeaning":word.get("primaryMeaning"),
        "smeaning":word.get("secondaryMeaning"),
        "wordlist":word.get("wordlist"),
        "sublist":word.get("sublist"),
        "phrase":word.get("phrase"),
        "sentenceId": _.find(wordHasSentenceData,function(whs){ return whs.get("wordId")==word.id;}).get("sentenceId"),
        "imageId": _.find(wordHasImageData,function(whi){ return whi.get("wordId")==word.id;}).get("imageId")

      };
    });



    var temp2 = _.map(temp1,function(word){
      return {
        "wordId":word.wordId,
        "word":word.word,
        "pmeaning":word.pmeaning,
        "smeaning":word.smeaning,
        "wordlist":word.wordlist,
        "sublist":word.sublist,
        "phrase":word.phrase,
        "sentenceId":word.sentenceId,
        "sentence": _.find(sentenceData,function(sentence){
          return sentence.id == word.sentenceId;
        }).get("content"),
        "image": _.find(imageData,function(image){
          return image.id == word.imageId;
        }).get("imageOriginal")
      }
    });


    res.success(temp2);

  },function(error){
    console.log(error);
  });


});